#define _CRT_SECURE_NO_WARNINGS
#define N 5
#include <stdio.h>
#include <time.h>

int main()
{
	srand(time(NULL));
	char arr[N][256];
	int i = 0, j = 0;
	int strcount;
	while (i < N)
	{
		fgets(arr[i], 256, stdin);
		i++;
	}
	strcount = rand() % (i - 1);
	printf("Your number  \n");
	while (arr[strcount][j] > 10)
	{
		printf("%c", arr[strcount][j]);
		j++;
	}
	printf("\n");
	return 0;
}