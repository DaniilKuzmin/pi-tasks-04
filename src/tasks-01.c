#include<stdio.h>
#include<string.h>
#define N 5
#define M 256
int main()
{
	int i, j, theNumberOfCharacters;
	char str[5][256];
	printf("Enter the desired length of characters \n");
	for (i = 0; i<N; i++)
		fgets(str[i],255,stdin);
	printf("\n");
	printf("Reverse the order \n");
	for (i = N - 1; i>-1; i--)
	{
		theNumberOfCharacters = strlen(str[i]);
		for (j = 0; j<theNumberOfCharacters; j++)
			printf("%c", str[i][j]);
	}
	return 0;
}